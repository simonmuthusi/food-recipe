package net.simonmuthusi.food.tasks;

/**
 * Created by simonmuthusi@gmail.com on 23/10/15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import net.simonmuthusi.food.R;
import net.simonmuthusi.food.parsers.JSONParser;
import net.simonmuthusi.food.settings.Recipe;
import net.simonmuthusi.food.settings.Settings;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RecipeItemTask extends AsyncTask<ArrayList<String>, Void,String> {
    Context context;
    String recipe_id;
    View frag;
    boolean search;
    public RecipeItemTask(Context context,String recipe_id,View frag)
    {
        this.context = context;
        this.recipe_id = recipe_id;
        this.frag = frag;
        search = true;
    }
    public void setSearch(boolean search)
    {
       this.search = search;
    }
    public boolean getSearch()
    {
        return search;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(ArrayList<String>... arg0) {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        JSONParser sh = new JSONParser();
        String jsonStr = sh.makeServiceCall(Settings.url+"get?key="+Settings.app_key+"&rId="+recipe_id, JSONParser.POST,params);
        String results = "<p><strong>Ingredients</strong></p>";

        if(jsonStr!=null)
        {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);

                String json = jsonObj.getString("recipe");
                JSONObject json_in = new JSONObject(json);
                String ingre = json_in.getString("ingredients");

                JSONArray msgs = json_in.getJSONArray("ingredients");


                for (int i = 0; i < msgs.length(); i++) {
                    results+="<p>&nbsp;&nbsp;&nbsp;&nbsp;* "+msgs.getString(i)+"</p>";
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return results;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        TextView ingredients = (TextView)frag.findViewById(R.id.ingredients);
        ingredients.setText(Html.fromHtml(result));

    }

}

