//package net.simonmuthusi.food.tasks;
//
//import android.content.Context;
//import android.os.AsyncTask;
//
//import net.simonmuthusi.food.parsers.JSONParser;
//import net.simonmuthusi.food.settings.Recipe;
//import net.simonmuthusi.food.settings.Settings;
//
//import org.apache.http.NameValuePair;
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by simonmuthusi@gmail.com on 23/10/15.
// */
////public class RecipeTask extends AsyncTask<Void, Void, ArrayList<Recipe>> {
////    private Context context;
////    private Settings setting;
////    private int page;
////    private String TAG_RECIPE = "recipes";
////    private String TAG_PUBLISHER = "publisher";
////    private String TAG_F2F_URL = "f2f_url";
////    private String TAG_TITLE = "title";
////    private String TAG_SOURCE_URL = "source_url";
////    private String TAG_RECIPE_ID = "recipe_id";
////    private String TAG_IMAGE_URL = "image_url";
////    private String TAG_SOCIAL_RANK = "social_rank";
////    private String TAG_PUBLISHER_URL = "publisher_url";
////
////    public RecipeTask(Context context){
////        this.context = context;
////        this.page = 1;
////    }
////
////    @Override
////    protected void onPreExecute() {
////        super.onPreExecute();
////    }
////
////    @Override
////    protected ArrayList<Recipe> doInBackground(Void... arg0) {
////        List<NameValuePair> params = new ArrayList<NameValuePair>();
////        JSONParser sh = new JSONParser();
////
////        ArrayList<Recipe> recipes = new ArrayList<Recipe>();;
////
////        String jsonStr = sh.makeServiceCall(Settings.url+"search?key="+Settings.app_key+"&page="+page, JSONParser.POST,params);
////        if(jsonStr!=null)
////        {
////            try {
////                JSONObject jsonObj = new JSONObject(jsonStr);
////                // Getting JSON Array node
////                JSONArray msgs = jsonObj.getJSONArray(TAG_RECIPE);
////
////                for (int i = 0; i < msgs.length(); i++) {
////                    JSONObject msg = msgs.getJSONObject(i);
////                    Recipe recipe = new Recipe();
////                    recipe.setPublisher(msg.getString(TAG_PUBLISHER));
////                    recipe.setF2f_url(msg.getString(TAG_F2F_URL));
////                    recipe.setTitle(msg.getString(TAG_TITLE));
////                    recipe.setSource_url(msg.getString(TAG_SOURCE_URL));
////                    recipe.setRecipe_id(msg.getString(TAG_RECIPE_ID));
////                    recipe.setImage_url(msg.getString(TAG_IMAGE_URL));
////                    recipe.setSocial_rank(msg.getString(TAG_SOCIAL_RANK));
////                    recipe.setPublisher_url(msg.getString(TAG_PUBLISHER_URL));
////                    // add recipe
////                    recipes.add(recipe);
////                }
////            }
////            catch(Exception e)
////            {
////                e.printStackTrace();
////            }
////        }
////
////        return recipes;
////    }
////
////    @Override
////    protected void onPostExecute(ArrayList<Recipe> result) {
////        super.onPostExecute(result);
////        JSONArray responseArr = new JSONArray();
////
////    }
////
////}
