package net.simonmuthusi.food.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import net.simonmuthusi.food.settings.RecipeItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by simonmuthusi@gmail.com on 28/10/15.
 */
public class RecipeModel extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "arecipe";

    // Message table name
    private static final String TABLE_RECIPE = "arecipe";

    // Message Table Columns names
    private final String IMAGE_URL = "image_url";
    private final String SOURCE_URL = "source_url";
    private final String F2F_URL = "f2f_url";
    private final String TITLE = "title";
    private final String PUBLISHER = "publisher";
    private final String PUBLISHER_URL = "publisher_url";
    private final String SOCIAL_RANK = "social_rank";
    private final String INGREDIENTS = "ingredients";
    private final String RECIPE_ID = "recipe_id";

    public RecipeModel(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_MESSAGE_TABLE = "CREATE TABLE '"+TABLE_RECIPE+"' ('"+
                IMAGE_URL+"' TEXT,'"+SOURCE_URL+"' TEXT,'"+F2F_URL+"' TEXT,'"+TITLE+"' TEXT,'"+
                PUBLISHER+"' TEXT,'"+PUBLISHER_URL+"' TEXT,'"+SOCIAL_RANK+"' TEXT,'"+INGREDIENTS+"' TEXT, '"+
                RECIPE_ID+"' TEXT)";
        db.execSQL(CREATE_MESSAGE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECIPE);

        // Create tables again
        onCreate(db);
    }

    // Adding new contact
    public void addRecipe(RecipeItem recipe) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(IMAGE_URL, recipe.getImage_url());
        values.put(SOURCE_URL, recipe.getSource_url());
        values.put(F2F_URL, recipe.getF2f_url());
        values.put(TITLE, recipe.getTitle());
        values.put(PUBLISHER, recipe.getPublisher());
        values.put(PUBLISHER_URL, recipe.getPublisher_url());
        values.put(SOCIAL_RANK, recipe.getSocial_rank());
        values.put(INGREDIENTS, recipe.getIngredients());
        values.put(RECIPE_ID, recipe.getRecipe_id());


        // Inserting Row
        db.insert(TABLE_RECIPE, null, values);
//        db.close(); // Closing database connection
    }

    // Getting single message
    public ArrayList<RecipeItem> getRecipe(String recipe_id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_RECIPE, new String[] { IMAGE_URL,
                        SOURCE_URL, F2F_URL, TITLE, PUBLISHER, PUBLISHER_URL, SOCIAL_RANK, INGREDIENTS, RECIPE_ID }, RECIPE_ID + "=?",
                new String[] { String.valueOf(recipe_id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();


        ArrayList<RecipeItem> recis = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                RecipeItem rec = new RecipeItem();
                rec.setImage_url(cursor.getString(0).toString());
                rec.setSource_url(cursor.getString(1).toString());
                rec.setF2f_url(cursor.getString(2).toString());
                rec.setTitle(cursor.getString(3).toString());
                rec.setPublisher(cursor.getString(4).toString());
                rec.setPublisher_url(cursor.getString(5).toString());
                rec.setSocial_rank(cursor.getString(6).toString());
                rec.setIngredients(cursor.getString(7).toString());
                rec.setRecipe_id(cursor.getString(8).toString());
                recis.add(rec);
            } while (cursor.moveToNext());
        }

        // return message
        return recis;
    }

    // Getting All Message
    public List<RecipeItem> getAllRecipes() {
        List<RecipeItem> recipeList = new ArrayList<RecipeItem>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_RECIPE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
//                Log.e("DATA_ITEM", cursor.getString(0).toString());
                RecipeItem rec = new RecipeItem();
                rec.setImage_url(cursor.getString(0).toString());
                rec.setSource_url(cursor.getString(1).toString());
                rec.setF2f_url(cursor.getString(2).toString());
                rec.setTitle(cursor.getString(3).toString());
                rec.setPublisher(cursor.getString(4).toString());
                rec.setPublisher_url(cursor.getString(5).toString());
                rec.setSocial_rank(cursor.getString(6).toString());
                rec.setIngredients(cursor.getString(7).toString());
                rec.setRecipe_id(cursor.getString(8).toString());
                // Adding contact to list
                recipeList.add(rec);
            } while (cursor.moveToNext());
        }
        // return message list
        return recipeList;
    }


    // Getting messages Count
    public int getRecipeCount() {
        String countQuery = "SELECT  * FROM " + TABLE_RECIPE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        // return count
        return cursor.getCount();
    }

    // check whether a recipe exists
    public boolean recipeExists(String recipe_id)
    {
        Cursor cursor = null;
        boolean found = false;
        SQLiteDatabase db = this.getReadableDatabase();
        String sql ="SELECT "+RECIPE_ID+" FROM "+TABLE_RECIPE+" WHERE "+RECIPE_ID+"='"+recipe_id+"'";
        cursor= db.rawQuery(sql,null);
        if(cursor.getCount()>0)
        {
            found = true;
        }
        else
        {
            found = false;
        }
        db.close();

        return found;
    }

    // Updating single message
    public int updateRecipe(RecipeItem recipe) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(IMAGE_URL, recipe.getImage_url());
        values.put(SOURCE_URL, recipe.getSource_url());
        values.put(F2F_URL, recipe.getF2f_url());
        values.put(TITLE, recipe.getTitle());
        values.put(PUBLISHER, recipe.getPublisher());
        values.put(PUBLISHER_URL, recipe.getPublisher_url());
        values.put(SOCIAL_RANK, recipe.getSocial_rank());
        values.put(INGREDIENTS, recipe.getIngredients());
        // updating row
        return db.update(TABLE_RECIPE, values, RECIPE_ID + " = ?",
                new String[]{String.valueOf(recipe.getRecipe_id())});
    }

    // Deleting single message
    public void deleteRecipe(RecipeItem recipe) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RECIPE, RECIPE_ID + " = ?",
                new String[]{String.valueOf(recipe.getRecipe_id())});
        db.close();
    }

    // delete all
    // Deleting single message
    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RECIPE, null, null);
        db.close();
    }
    // delete message by ID
    public void deleteRecipe(String recipe_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RECIPE, RECIPE_ID + "=?", new String[]{recipe_id});
        db.close();
    }

}

