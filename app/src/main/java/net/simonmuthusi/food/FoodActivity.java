package net.simonmuthusi.food;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
//import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gc.materialdesign.views.ButtonFlat;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import net.simonmuthusi.food.fragments.AboutFragment;
import net.simonmuthusi.food.fragments.OnboardingFragment1;
import net.simonmuthusi.food.fragments.OnboardingFragment2;
import net.simonmuthusi.food.fragments.OnboardingFragment3;
import net.simonmuthusi.food.fragments.RecipeFragment;
import net.simonmuthusi.food.fragments.SavedRecipeFragment;

public class FoodActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private ViewPager pager;
    private SmartTabLayout indicator;
    private ButtonFlat skip;
    private ButtonFlat next;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Get the shared preferences
        preferences =  getSharedPreferences("my_preferences", MODE_PRIVATE);
//        preferences.edit()
//                .putBoolean("onboarding_complete",false).apply();

// Check if onboarding_complete is false
        if(!preferences.getBoolean("onboarding_complete",false)) {

            setContentView(R.layout.onboard_one);
            pager = (ViewPager)findViewById(R.id.pager);
            indicator = (SmartTabLayout)findViewById(R.id.indicator);
            skip = (ButtonFlat)findViewById(R.id.skip);
            next = (ButtonFlat)findViewById(R.id.next);

            FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
                @Override
                public Fragment getItem(int position) {
                    switch (position) {
                        case 0 : return new OnboardingFragment1();
                        case 1 : return new OnboardingFragment2();
                        case 2 : return new OnboardingFragment3();
                        default: return null;
                    }
                }

                @Override
                public int getCount() {
                    return 2;
                }
            };

            pager.setAdapter(adapter);
            indicator.setViewPager(pager);

            skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finishOnboarding();
                }
            });

            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pager.getCurrentItem() == 1) { // The last screen
                        finishOnboarding();
                    } else {
                        pager.setCurrentItem(
                                pager.getCurrentItem() + 1,
                                true
                        );
                    }
                }
            });


            indicator.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    if (position == 1) {
                        skip.setVisibility(View.GONE);
                        next.setText("Done");
                    } else {
                        skip.setVisibility(View.VISIBLE);
                        next.setText("Next");
                    }
                }
            });
//            // Start the onboarding Activity
//            Intent onboarding = new Intent(this, FoodActivity.class);
//            startActivity(onboarding);
//            Log.e("COMPLETE", "Not Completed");
//
//            // Close the main Activity
//            finish();
//            return;
        }
        else
        {
            setContentView(R.layout.activity_food);
            mNavigationDrawerFragment = (NavigationDrawerFragment)
                    getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
            mTitle = getTitle();

            // Set up the drawer.
            mNavigationDrawerFragment.setUp(
                    R.id.navigation_drawer,
                    (DrawerLayout) findViewById(R.id.drawer_layout));
            Log.e("COMPLETE", "Completed");
        }








//
//        mNavigationDrawerFragment = (NavigationDrawerFragment)
//                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
//        mTitle = getTitle();
//
//        // Set up the drawer.
//        mNavigationDrawerFragment.setUp(
//                R.id.navigation_drawer,
//                (DrawerLayout) findViewById(R.id.drawer_layout));

  }
    private void finishOnboarding() {
        // Get the shared preferences
        SharedPreferences preferences =
                getSharedPreferences("my_preferences", MODE_PRIVATE);

        // Set onboarding_complete to true
        preferences.edit()
                .putBoolean("onboarding_complete",true).apply();

        // Launch the main Activity, called MainActivity
        Intent main = new Intent(this, FoodActivity.class);
        startActivity(main);

        // Close the OnboardingActivity
        finish();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Fragment fr;
        if(position==0)
        {
            mTitle = getString(R.string.food_recipe);
            fr = new RecipeFragment();
        }
        else if(position==1)
        {
            mTitle = getString(R.string.saved_recipe);
            fr = new SavedRecipeFragment();
        }
        else if(position==2)
        {
            mTitle = getString(R.string.about_recipe);
            fr = new AboutFragment();
        }
        else
        {
            mTitle = getString(R.string.food_recipe);
            fr = new RecipeFragment();
        }
        FragmentManager fm = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.container, fr);
        fragmentTransaction.
                addToBackStack(null).commit();
    }

    public void onSectionAttached(int number)
    {
        switch (number)
        {
            case 1:
                mTitle = getString(R.string.food_recipe);
                break;
            case 2:
                mTitle = getString(R.string.saved_recipe);
                break;
            case 3:
                mTitle = getString(R.string.about_recipe);
                break;
            default:
                mTitle = getString(R.string.food_recipe);
                break;
        }


    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(preferences.getBoolean("onboarding_complete",false)) {
            if (!mNavigationDrawerFragment.isDrawerOpen()) {
                // Only show items in the action bar relevant to this screen
                // if the drawer is not showing. Otherwise, let the drawer
                // decide what to show in the action bar.
                getMenuInflater().inflate(R.menu.food, menu);
                restoreActionBar();
                return true;
            }

            return super.onCreateOptionsMenu(menu);
        }
        else
        {
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_food, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((FoodActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
