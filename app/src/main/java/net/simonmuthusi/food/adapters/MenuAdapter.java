package net.simonmuthusi.food.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.simonmuthusi.food.R;

/**
 * Created by simonmuthusi@gmail.com on 27/10/15.
 */
public class MenuAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] titles;
    public MenuAdapter(Activity context, String[] titles) {
        super(context, R.layout.main_menu,titles );
        this.context=context;
        this.titles=titles;
    }
    public View getView(int position,View view,ViewGroup parent) {

        LayoutInflater inflater=context.getLayoutInflater();

        View rowView=inflater.inflate(R.layout.main_menu, null, true);

        ImageView image = (ImageView) rowView.findViewById(R.id.mnuImage);
        TextView text = (TextView) rowView.findViewById(R.id.mnuText);
        if(titles[position]==context.getString(R.string.food_recipe))
        {
            image.setImageResource(R.drawable.food_menu);
            text.setText(context.getString(R.string.food_recipe));
        }
        else if(titles[position]==context.getString(R.string.saved_recipe))
        {
            image.setImageResource(R.drawable.saved_menu);
            text.setText(context.getString(R.string.saved_recipe));
        }
        else if(titles[position]==context.getString(R.string.about_recipe))
        {
            image.setImageResource(R.drawable.about_menu);
            text.setText(context.getString(R.string.about_recipe));
        }
        else
        {
            image.setImageResource(R.drawable.food_menu);
            text.setText(context.getString(R.string.food_recipe));;
        }
        return rowView;
    };
}
