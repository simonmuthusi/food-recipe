package net.simonmuthusi.food.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.simonmuthusi.food.R;
import net.simonmuthusi.food.models.RecipeModel;
import net.simonmuthusi.food.parsers.ImageLoader;
import net.simonmuthusi.food.settings.Recipe;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by simonmuthusi@gmail.com on 23/10/15.
 */
public class RecipeAdapter extends ArrayAdapter<Recipe> implements Filterable {
    private final Activity context;
    private ArrayList<Recipe> recipes,s_recipes;
    private ImageLoader imageLoader;
    private RecipeModel recipe_model;
    private Filter myFilter;

    public RecipeAdapter(Activity context, ArrayList<Recipe> recipes) {
        super(context, R.layout.recipe_item_url,recipes );
        this.context=context;
        this.recipes=recipes;
        this.s_recipes=recipes;
        this.s_recipes.addAll(recipes);
        this.recipe_model = new RecipeModel(context);

    }
    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.recipe_item_url, null, true);
        imageLoader = new ImageLoader(context);

        TextView title = (TextView) rowView.findViewById(R.id.title);
        TextView publisher = (TextView) rowView.findViewById(R.id.publisher);
        TextView social_rank = (TextView) rowView.findViewById(R.id.social_rank);
        ImageView pin = (ImageView) rowView.findViewById(R.id.pin);

        // set even the hidden
        TextView f2f_url = (TextView) rowView.findViewById(R.id.f2f_url);
        TextView source_url = (TextView) rowView.findViewById(R.id.source_url);
        TextView recipe_id = (TextView) rowView.findViewById(R.id.recipe_id);
        TextView image_url = (TextView) rowView.findViewById(R.id.image_url);
        TextView publisher_url = (TextView) rowView.findViewById(R.id.publisher_url);

        ImageView image = (ImageView) rowView.findViewById(R.id.imgRecipe);

        title.setText(recipes.get(position).getTitle());
        publisher.setText(recipes.get(position).getPublisher());
        social_rank.setText(recipes.get(position).getSocial_rank());

        if(recipe_model.recipeExists(recipes.get(position).getRecipe_id()))
        {
            pin.setImageResource(R.drawable.pinned);
        }
        else
        {
            pin.setImageResource(R.drawable.pin);
        }
        pin.setClickable(true);

        // set even the hidden
        f2f_url.setText(recipes.get(position).getF2f_url());
        source_url.setText(recipes.get(position).getSource_url());
        recipe_id.setText(recipes.get(position).getRecipe_id());
        image_url.setText(recipes.get(position).getImage_url());
        publisher_url.setText(recipes.get(position).getPublisher_url());

        imageLoader.DisplayImage(recipes.get(position).getImage_url(), image);

        pin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(context,"Clicked",Toast.LENGTH_LONG);
            }
        });



        ////////////////////////////////////////////////////////////////////////////////////////////
        myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                ArrayList<Recipe> tempList=new ArrayList<Recipe>();
//                s_recipes = recipes;
                //constraint is the result from text you want to filter against.
                //objects is your data set you will filter from
                if(constraint != null && s_recipes!=null) {
                    int length=s_recipes.size();
                    int i=0;
                    while(i<length){
                        Recipe item=s_recipes.get(i);
                        //do whatever you wanna do here
                        //adding result set output array
                        Log.e("SEARCH","SEARCH:"+constraint.toString().toLowerCase()+"\nTITLE:"+item.getTitle());
                        if(item.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())){
                            tempList.add(item);
                        }

                        i++;
                    }
                    //following two lines is very important
                    //as publish result can only take FilterResults objects

                }
                else
                {
                    tempList = recipes;
                }
                filterResults.values = tempList;
                filterResults.count = tempList.size();
                Log.e("RESULTS_SIZE",tempList.size()+"");
                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence contraint, FilterResults results) {
                s_recipes = (ArrayList<Recipe>) results.values;
//                if (results.count > 0) {
//                    notifyDataSetChanged();
//                } else {
//                    notifyDataSetInvalidated();
//                }
                s_recipes = (ArrayList)results.values;
                notifyDataSetChanged();
                clear();
                for(int i = 0, l = s_recipes.size(); i < l; i++)
                {
                    add(s_recipes.get(i));
                notifyDataSetInvalidated();
            }

            }
        };
        ////////////////////////////////////////////////////////////////////////////////////////////
        return rowView;
    }










//    // Filter Class
//    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        s_recipes.clear();
//        if (charText.length() == 0) {
//            s_recipes.addAll(recipes);
//        } else {
//            for (Recipe wp : recipes) {
//                if (wp.getTitle().toLowerCase(Locale.getDefault())
//                        .contains(charText)) {
//                    s_recipes.add(wp);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
@Override
public Filter getFilter() {
    return myFilter;
}

    }
