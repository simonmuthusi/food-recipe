package net.simonmuthusi.food.fragments;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.simonmuthusi.food.R;
import net.simonmuthusi.food.adapters.RecipeAdapter;
import net.simonmuthusi.food.models.RecipeModel;
import net.simonmuthusi.food.parsers.JSONParser;
import net.simonmuthusi.food.settings.Recipe;
import net.simonmuthusi.food.settings.RecipeItem;
import net.simonmuthusi.food.settings.Settings;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by simonmuthusi@gmail.com on 23/10/15.
 */
public class RecipeFragment extends ListFragment  implements AdapterView.OnItemLongClickListener {
    private ListView layout;
    RecipeAdapter adapter;
    ArrayList<Recipe> recipes;
    View frag;
    private int page = 1;
    Button load_more;
    TextView inputSearch;
    ArrayList<Recipe> all_results = new ArrayList<>();
    RecipeModel recipeModel;
    boolean search;
    private String search_key;
    public RecipeFragment()
    {
        page = 1;
        all_results = new ArrayList<>();
        recipeModel = new RecipeModel(getActivity());
        search = false;
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        recipeModel = new RecipeModel(getActivity());
        search = false;
//        recipeModel.deleteAll();

        if(frag==null) {
            //Inflate the layout for this fragment
            frag = inflater.inflate(
                    R.layout.recipe_fragment, container, false);

            layout = (ListView) frag.findViewById(R.id.list);
            layout.setTextFilterEnabled(true);
            inputSearch = (TextView)frag.findViewById(R.id.inputSearch);
//        get all recipes here
            recipes = new ArrayList<Recipe>();

            new RecipeTask(getActivity()).execute();

            adapter = new RecipeAdapter(getActivity(), recipes);
            layout.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                }
            });
            inputSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        search = true;
                        search_key = inputSearch.getText().toString();
                        page = 1;
                        new RecipeTask(getActivity()).execute();
                        load_more.setText("crunching data. please wait ...");
                        load_more.setTextColor(Color.WHITE);
                        load_more.setBackgroundColor(Color.BLACK);
                        load_more.setClickable(false);
                        frag.findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
                        layout.setVisibility(View.GONE);

//                        performSearch();
                        return true;
//                    }
//                    return false;
                }
            });

            inputSearch.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                    // When user changed the Text
                    if(cs.toString().length()==0)
                    {
                        page = 1;
                        search = false;
                    }
//                    adapter.getFilter().filter(cs);
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                              int arg3) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                }
            });

            // Creating bottom button as a footer view
//            Button bAddNew = (Button) getActivity().getLayoutInflater().inflate(R.layout.footer, null);
//            bAddNew.setText("Add Routine");

            View footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer, null, false);
//            layout = getListView();
            layout.addFooterView(footerView);

//            layout = getListView();   // Get reference to ListActivities ListView
//            layout.addFooterView(bAddNew);



            setListAdapter(adapter);

//            button load more
            load_more = (Button)frag.findViewById(R.id.load_more);
            frag.findViewById(R.id.load_more).setVisibility(View.GONE);
            load_more.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    page = page+1;
                    new RecipeTask(getActivity()).execute();
                    load_more.setText("crunching data. please wait ...");
                    load_more.setTextColor(Color.WHITE);
                    load_more.setBackgroundColor(Color.BLACK);
                    load_more.setClickable(false);
                    frag.findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
                    layout.setVisibility(View.GONE);
                }
            });

        }

        return frag;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        super.onListItemClick(l, v, position, id);
//        Toast.makeText(getActivity(), "Field Vaasdasdaccant",    Toast.LENGTH_LONG).show();
        Fragment fr;
        fr = new RecipeItemFragment();

        // get all parameters and pass them as variables
        TextView publisher = (TextView) v.findViewById(R.id.publisher);
        TextView f2f_url = (TextView) v.findViewById(R.id.f2f_url);
        TextView title = (TextView) v.findViewById(R.id.title);
        TextView source_url = (TextView) v.findViewById(R.id.source_url);
        TextView recipe_id = (TextView) v.findViewById(R.id.recipe_id);
        TextView image_url  = (TextView) v.findViewById(R.id.image_url);
        TextView publisher_url = (TextView) v.findViewById(R.id.publisher_url);
        TextView social_rank = (TextView) v.findViewById(R.id.social_rank);

        Bundle bundle = new Bundle();
        bundle.putString("title", title.getText().toString());
        bundle.putString("publisher", publisher.getText().toString());
        bundle.putString("f2f_url", f2f_url.getText().toString());
        bundle.putString("source_url", source_url.getText().toString());
        bundle.putString("recipe_id", recipe_id.getText().toString());
        bundle.putString("image_url", image_url.getText().toString());
        bundle.putString("publisher_url", publisher_url.getText().toString());
        bundle.putString("social_rank", social_rank.getText().toString());
//        bundle.putString("image", image.get);
        fr.setArguments(bundle);

        android.support.v4.app.FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.container, fr);
        fragmentTransaction.
        addToBackStack(null).commit();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerForContextMenu(getListView());
//        registerForContextMenu((ListView)view.findViewById(R.id.list));
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getListView().setOnItemLongClickListener(this);

        MenuInflater menuInflater = getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.recipe_url_menu, menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        Recipe recipe;
        RecipeItem rec_item;

        switch (item.getItemId()) {
            case R.id.save_menu:
//                save menu
                recipe = (Recipe) getListAdapter().getItem(info.position);
                rec_item = new RecipeItem();

                rec_item.setImage_url(recipe.getImage_url());
                rec_item.setSource_url(recipe.getSource_url());
                rec_item.setF2f_url(recipe.getF2f_url());
                rec_item.setTitle(recipe.getTitle());
                rec_item.setPublisher(recipe.getPublisher());
                rec_item.setPublisher_url(recipe.getPublisher_url());
                rec_item.setSocial_rank(recipe.getSocial_rank());
                rec_item.setIngredients("");
                rec_item.setRecipe_id(recipe.getRecipe_id());

                recipeModel.addRecipe(rec_item);
                adapter.notifyDataSetChanged();

                Toast.makeText(getActivity().getApplicationContext(), "Saved successfully", Toast.LENGTH_SHORT);
                return true;
            case R.id.unsave_menu:
//                save menu
                recipe = (Recipe) getListAdapter().getItem(info.position);
                rec_item = new RecipeItem();

                rec_item.setImage_url(recipe.getImage_url());
                rec_item.setSource_url(recipe.getSource_url());
                rec_item.setF2f_url(recipe.getF2f_url());
                rec_item.setTitle(recipe.getTitle());
                rec_item.setPublisher(recipe.getPublisher());
                rec_item.setPublisher_url(recipe.getPublisher_url());
                rec_item.setSocial_rank(recipe.getSocial_rank());
                rec_item.setIngredients("");
                rec_item.setRecipe_id(recipe.getRecipe_id());

                recipeModel.deleteRecipe(recipe.getRecipe_id());
                adapter.notifyDataSetChanged();

                Toast.makeText(getActivity().getApplicationContext(), "Deleted successfully", Toast.LENGTH_SHORT);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
//        Toast.makeText(getActivity(),"selected",Toast.LENGTH_LONG);
        return false;
    }

    public class RecipeTask extends AsyncTask<Void, Void, ArrayList<Recipe>> {
        private Context context;
        private Settings setting;

        private String TAG_RECIPE = "recipes";
        private String TAG_PUBLISHER = "publisher";
        private String TAG_F2F_URL = "f2f_url";
        private String TAG_TITLE = "title";
        private String TAG_SOURCE_URL = "source_url";
        private String TAG_RECIPE_ID = "recipe_id";
        private String TAG_IMAGE_URL = "image_url";
        private String TAG_SOCIAL_RANK = "social_rank";
        private String TAG_PUBLISHER_URL = "publisher_url";

        public RecipeTask(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected ArrayList<Recipe> doInBackground(Void... arg0) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONParser sh = new JSONParser();

            ArrayList<Recipe> recipes_ = new ArrayList<Recipe>();

            String jsonStr = null;
            if(search)
            {
                jsonStr = sh.makeServiceCall(Settings.url + "search?key=" + Settings.app_key + "&q=" + search_key+"&page="+page, JSONParser.POST, params);
            }
            else
            {
                jsonStr = sh.makeServiceCall(Settings.url + "search?key=" + Settings.app_key + "&page=" + page, JSONParser.POST, params);
            }
            if(jsonStr!=null)
            {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    // Getting JSON Array node
                    JSONArray msgs = jsonObj.getJSONArray(TAG_RECIPE);

                    for (int i = 0; i < msgs.length(); i++) {
                        JSONObject msg = msgs.getJSONObject(i);
                        Recipe recipe = new Recipe();
                        recipe.setPublisher(msg.getString(TAG_PUBLISHER));
                        recipe.setF2f_url(msg.getString(TAG_F2F_URL));
                        recipe.setTitle(msg.getString(TAG_TITLE));
                        recipe.setSource_url(msg.getString(TAG_SOURCE_URL));
                        recipe.setRecipe_id(msg.getString(TAG_RECIPE_ID));
                        recipe.setImage_url(msg.getString(TAG_IMAGE_URL));
                        recipe.setSocial_rank(msg.getString(TAG_SOCIAL_RANK));
                        recipe.setPublisher_url(msg.getString(TAG_PUBLISHER_URL));
                        // add recipe
                        recipes_.add(recipe);
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            recipes = recipes_;
            return recipes_;
        }

        @Override
        protected void onPostExecute(ArrayList<Recipe> results) {
            super.onPostExecute(results);

            all_results.addAll(results);
            adapter = new RecipeAdapter(getActivity(),results);
            layout.setTextFilterEnabled(true);

            setListAdapter(adapter);
            adapter.notifyDataSetChanged();
            load_more.setClickable(true);
            load_more.setText("Load more ...");
            load_more.setTextColor(Color.BLACK);
            load_more.setBackgroundColor(Color.GRAY);
            frag.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
            layout.setVisibility(View.VISIBLE);
            frag.findViewById(R.id.load_more).setVisibility(View.VISIBLE);


        }

    }

}
