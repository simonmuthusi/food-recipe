package net.simonmuthusi.food.fragments;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.simonmuthusi.food.R;
import net.simonmuthusi.food.parsers.ImageLoader;
import net.simonmuthusi.food.tasks.RecipeItemTask;

/**
 * Created by simonmuthusi@gmail.com on 25/10/15.
 */
public class RecipeItemFragment extends Fragment {
    private ImageLoader imageLoader;
    private View frag;
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        if(frag==null) {
            //Inflate the layout for this fragment
            frag = inflater.inflate(
                    R.layout.recipe_item, container, false);

            imageLoader = new ImageLoader(getActivity());

            Bundle bundle = this.getArguments();
            String str_title = bundle.getString("title", "");
            String str_publisher = bundle.getString("publisher", "");
            String str_f2f_url = bundle.getString("f2f_url", "");
            final String str_source_url = bundle.getString("source_url", "");
            String str_recipe_id = bundle.getString("recipe_id", "");
            String str_image_url = bundle.getString("image_url", "");
            String str_social_rank = bundle.getString("social_rank", "");
            String str_publisher_url = bundle.getString("publisher_url", "");
            String str_ingridients = "loading ingredients ...";


            final TextView title = (TextView) frag.findViewById(R.id.title);
            TextView publisher = (TextView) frag.findViewById(R.id.publisher);
            TextView social_rank = (TextView) frag.findViewById(R.id.social_rank);
            ImageView img = (ImageView) frag.findViewById(R.id.imgRecipe);
            final TextView ingredients = (TextView) frag.findViewById(R.id.ingredients);
            TextView steps = (TextView) frag.findViewById(R.id.steps);
            ImageView share = (ImageView) frag.findViewById(R.id.social_fb);


            // set even the hidden
            TextView f2f_url = (TextView) frag.findViewById(R.id.f2f_url);
            TextView source_url = (TextView) frag.findViewById(R.id.source_url);
            TextView recipe_id = (TextView) frag.findViewById(R.id.recipe_id);
            TextView publisher_url = (TextView) frag.findViewById(R.id.publisher_url);

            imageLoader.DisplayImage(str_image_url, img);
            title.setText(str_title);
            publisher.setText(str_publisher);
            social_rank.setText(Html.fromHtml("Social Rank<br/><center>" + str_social_rank + "</center>"));
            f2f_url.setText(str_f2f_url);
            source_url.setText(str_source_url);
            recipe_id.setText(str_recipe_id);
            ingredients.setText(Html.fromHtml(str_ingridients));
            publisher_url.setText(Html.fromHtml("<b>Publisher:</b> " + str_publisher_url));

            steps.setClickable(true);
            steps.setMovementMethod(LinkMovementMethod.getInstance());
            String text = "<a href='" + str_source_url + "'> View steps here </a>";
            steps.setText(Html.fromHtml(text));

            share.setClickable(true);
            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    share info now
                    String message = "From Food Recipe App. Recipe: "+title.getText().toString()+"  "+str_source_url;
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.putExtra(Intent.EXTRA_TEXT, message);

                    startActivity(Intent.createChooser(share, "Title of the dialog the system will open"));
                }
            });


            new RecipeItemTask(getActivity(), str_recipe_id, frag).execute();
        }


        return frag;
    }
}

