package net.simonmuthusi.food.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.simonmuthusi.food.R;

/**
 * Created by simonmuthusi@gmail.com on 16/11/15.
 */
public class AboutFragment extends Fragment
{
    private View frag;

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        if (frag == null) {
            //Inflate the layout for this fragment
            frag = inflater.inflate(
                    R.layout.about_fragment, container, false);
        }
        return frag;
    }
}
