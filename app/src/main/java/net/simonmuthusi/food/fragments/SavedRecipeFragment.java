package net.simonmuthusi.food.fragments;

//import android.app.ListFragment;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.simonmuthusi.food.R;
import net.simonmuthusi.food.adapters.RecipeAdapter;
import net.simonmuthusi.food.models.RecipeModel;
import net.simonmuthusi.food.parsers.JSONParser;
import net.simonmuthusi.food.settings.Recipe;
import net.simonmuthusi.food.settings.RecipeItem;
import net.simonmuthusi.food.settings.Settings;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by simonmuthusi@gmail.com on 23/10/15.
 */
public class SavedRecipeFragment extends ListFragment implements AdapterView.OnItemLongClickListener {
    private ListView layout;
    RecipeAdapter adapter;
    ArrayList<Recipe> recipes;
    View frag;
    private int page = 1;
    ArrayList<Recipe> all_results = new ArrayList<>();
    RecipeModel recipeModel;
    List<RecipeItem> recs;
    public SavedRecipeFragment()
    {
        recipeModel = new RecipeModel(getActivity());
        page = 1;
        all_results = new ArrayList<>();
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        recipeModel = new RecipeModel(getActivity());

        if(frag==null) {
            //Inflate the layout for this fragment
            frag = inflater.inflate(
                    R.layout.saved_recipe_fragment, container, false);

            layout = (ListView) frag.findViewById(R.id.list);
//        get all recipes here
            recipes = new ArrayList<Recipe>();
            //////////////////////////////////////////////////////
//            Recipe q1 = new Recipe();
//            q1.setTitle("one");
//            q1.setRecipe_id("1");
//            Recipe q2 = new Recipe();
//            q2.setTitle("Two");
//            q2.setRecipe_id("2");
//            recipes.add(q1);
//            recipes.add(q2);

            //////////////////////////////////////////////////////
            recs = recipeModel.getAllRecipes();
            for(int i=0;i<recs.size();i++)
            {
                Recipe r1 = new Recipe();
                r1.setImage_url(recs.get(i).getImage_url());
                r1.setPublisher(recs.get(i).getPublisher());
                r1.setF2f_url(recs.get(i).getF2f_url());
                r1.setTitle(recs.get(i).getTitle());
                r1.setSource_url(recs.get(i).getSource_url());
                r1.setRecipe_id(recs.get(i).getRecipe_id());
                r1.setSocial_rank(recs.get(i).getSocial_rank());
                r1.setPublisher_url(recs.get(i).getPublisher_url());
                recipes.add(r1);
            }

            adapter = new RecipeAdapter(getActivity(), recipes);
            layout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    Toast.makeText(getActivity(),"ehehehe",Toast.LENGTH_SHORT);
                    return false;
                }
            });
            registerForContextMenu(layout);
            layout.setOnItemLongClickListener(this);
            layout.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                }
            });

            setListAdapter(adapter);
        }

        return frag;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        super.onListItemClick(l, v, position, id);
//        Toast.makeText(getActivity(), "Field Vaasdasdaccant",    Toast.LENGTH_LONG).show();
        Fragment fr;
        fr = new RecipeItemFragment();

        // get all parameters and pass them as variables
        TextView publisher = (TextView) v.findViewById(R.id.publisher);
        TextView f2f_url = (TextView) v.findViewById(R.id.f2f_url);
        TextView title = (TextView) v.findViewById(R.id.title);
        TextView source_url = (TextView) v.findViewById(R.id.source_url);
        TextView recipe_id = (TextView) v.findViewById(R.id.recipe_id);
        TextView image_url  = (TextView) v.findViewById(R.id.image_url);
        TextView publisher_url = (TextView) v.findViewById(R.id.publisher_url);
        TextView social_rank = (TextView) v.findViewById(R.id.social_rank);

        Bundle bundle = new Bundle();
        bundle.putString("title", title.getText().toString());
        bundle.putString("publisher", publisher.getText().toString());
        bundle.putString("f2f_url", f2f_url.getText().toString());
        bundle.putString("source_url", source_url.getText().toString());
        bundle.putString("recipe_id", recipe_id.getText().toString());
        bundle.putString("image_url", image_url.getText().toString());
        bundle.putString("publisher_url", publisher_url.getText().toString());
        bundle.putString("social_rank", social_rank.getText().toString());
//        bundle.putString("image", image.get);
        fr.setArguments(bundle);

        android.support.v4.app.FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.container, fr);
        fragmentTransaction.
                addToBackStack(null).commit();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerForContextMenu(getListView());
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getListView().setOnItemLongClickListener(this);

        MenuInflater menuInflater = getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.recipe_url_menu, menu);

    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        Recipe recipe;
        RecipeItem rec_item;

        switch (item.getItemId()) {
            case R.id.save_menu:
//                save menu
                recipe = (Recipe) getListAdapter().getItem(info.position);
                rec_item = new RecipeItem();

                rec_item.setImage_url(recipe.getImage_url());
                rec_item.setSource_url(recipe.getSource_url());
                rec_item.setF2f_url(recipe.getF2f_url());
                rec_item.setTitle(recipe.getTitle());
                rec_item.setPublisher(recipe.getPublisher());
                rec_item.setPublisher_url(recipe.getPublisher_url());
                rec_item.setSocial_rank(recipe.getSocial_rank());
                rec_item.setIngredients("");
                rec_item.setRecipe_id(recipe.getRecipe_id());

                recipeModel.addRecipe(rec_item);
                adapter.notifyDataSetChanged();

                Toast.makeText(getActivity().getApplicationContext(), "Saved successfully", Toast.LENGTH_SHORT);
                return true;
            case R.id.unsave_menu:
//                save menu
                recipe = (Recipe) getListAdapter().getItem(info.position);
                rec_item = new RecipeItem();

                rec_item.setImage_url(recipe.getImage_url());
                rec_item.setSource_url(recipe.getSource_url());
                rec_item.setF2f_url(recipe.getF2f_url());
                rec_item.setTitle(recipe.getTitle());
                rec_item.setPublisher(recipe.getPublisher());
                rec_item.setPublisher_url(recipe.getPublisher_url());
                rec_item.setSocial_rank(recipe.getSocial_rank());
                rec_item.setIngredients("");
                rec_item.setRecipe_id(recipe.getRecipe_id());

                recipeModel.deleteRecipe(recipe.getRecipe_id());
                adapter.notifyDataSetChanged();

                Toast.makeText(getActivity().getApplicationContext(), "Deleted successfully", Toast.LENGTH_SHORT);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        return false;
    }
}

